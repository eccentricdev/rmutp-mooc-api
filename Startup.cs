using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using rmutp_mooc_api.Modules.Utilities.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using SeventyOneDev.Core.Context;
using SeventyOneDev.Utilities;
using SeventyOneDev.Core.Security.Models;
using SeventyOneDev.Core;
using Hangfire;
using Hangfire.Oracle.Core;
using Oracle.ManagedDataAccess.Client;
using System.Reflection;
using System.IO;
using Microsoft.OpenApi.Any;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.HttpOverrides;
using Swashbuckle.AspNetCore.SwaggerUI;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore.Infrastructure;
using rmutp_mooc_api.Modules.Commons.Initials;
using Microsoft.IdentityModel.Logging;
using rmutp_mooc_api.Modules.Test.Configures;
using rmutp_mooc_api.jwtConfig;
using rmutp_mooc_api.Modules.User.Interfaces;
using rmutp_mooc_api.Modules.User.Services;
using rmutp_mooc_api.Modules.User.Configures;
using rmutp_mooc_api.Modules.Subject.Configures;

namespace rmutp_mooc_api
{
    public class Startup
    {
      // readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
          services.AddControllersWithViews()
    .AddNewtonsoftJson(options =>
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);
          services.AddDbContext<Db>(options =>
            {
              options.UseNpgsql(Configuration["databases:postgres"]);
            },
            ServiceLifetime.Transient
          );
          // services.AddDbContext<DbAdmin>(options =>
          //   {
          //     options.UseNpgsql(Configuration["DatabaseConnections:Postgres_admin"]);
          //   },
          //   ServiceLifetime.Transient
          // );          services.AddDbContext<DbAdminEnrollment>(options =>
          //   {
          //     options.UseNpgsql(Configuration["DatabaseConnections:Postgres_etest_enrollment"]);
          //   },
          //   ServiceLifetime.Transient
          // );
          services.AddDbContext<SeventyOneDevCoreContext>(options =>
          {
            options.UseNpgsql(Configuration["databases:postgres"], 
              b => b.MigrationsAssembly("rmutp-mooc-api"));

          },ServiceLifetime.Transient);
                 services.AddControllers(o =>{
              o.Conventions.Add(
                new GenericControllerRouteConvention()
              );
            })
            .
            ConfigureApplicationPartManager(m =>
              {

                m.FeatureProviders.Add(new GenericTypeControllerFeatureProvider());
                m.FeatureProviders.Add(new GenericUidTypeControllerFeatureProvider());
                // m.FeatureProviders.Add(new GenericTestUidTypeControllerFeatureProvider());

              }
            ).AddJsonOptions(options=>
              options.JsonSerializerOptions.Converters.Add(new TimeSpanToStringConverter()));
            services.Configure<Setting>(Configuration.GetSection("SeventyOneDev.Core"));
            services.Configure<jwt_config>(Configuration.GetSection("jwt_config"));
            services.Configure<application_setting>(Configuration.GetSection("Settings"));
            
            services.AddScoped(typeof(EntityIdService<,>));
            services.AddScoped(typeof(EntityUidService<,>));
            services.AddScoped(typeof(ReadOnlyEntityUidService<>));
            
            var jwt = new jwt_Config()
            {
                secret_key = Configuration["jwt_config:secret"],
                audience = Configuration["jwt_config:audience"],
                issuer = Configuration["jwt_config:issuer"]
            };
            services.AddSingleton<jwt_Config>(jwt);   
            services.AddSubjectServices();
            services.AddTestServices();
            services.AddUserServices();
            services.AddSecurities();
            services.AddNotifications();
            services.AddDocuments();
            services.AddCors();
            services.AddSwaggerGen(c =>
            {
              c.DocumentFilter<CustomSwaggerFilter>();
              c.SwaggerDoc("subject", new OpenApiInfo { Title = "Subject API", Version = "v1" });
              c.SwaggerDoc("security", new OpenApiInfo { Title = "Security API", Version = "v1" });
              c.SwaggerDoc("test", new OpenApiInfo {Title = "Test API", Version = "v1"});
              c.SwaggerDoc("user", new OpenApiInfo {Title = "User API", Version = "v1"});
              c.SwaggerDoc("utilities", new OpenApiInfo {Title = "Utilities API", Version = "v1"});

              var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
              var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
              c.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
              c.EnableAnnotations();

              c.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
              c.MapType(typeof(TimeSpan?), () => new OpenApiSchema
              {
                Type = "string",
                Example = new OpenApiString("09:30:00")
              });
              c.MapType(typeof(TimeSpan), () => new OpenApiSchema
              {
                Type = "string",
                Example = new OpenApiString("09:30:00")
              });
              c.MapType(typeof(DateTime?), () => new OpenApiSchema
              {
                Type = "string",
                Example = new OpenApiString(DateTime.Now.ToString("yyyy-MM-dd"))
              });
              c.MapType(typeof(DateTime), () => new OpenApiSchema
              {
                Type = "string",
                Example = new OpenApiString(DateTime.Now.ToString("yyyy-MM-dd"))
              });
              //c.EnableAnnotations();
              c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
              {
                Type = SecuritySchemeType.Http,
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
                Scheme = "bearer",
                Description = "Please insert JWT token into field"
              });

              c.AddSecurityRequirement(new OpenApiSecurityRequirement
              {
                {
                  new OpenApiSecurityScheme
                  {
                    Reference = new OpenApiReference
                    {
                      Type = ReferenceType.SecurityScheme,
                      Id = "Bearer"
                    }
                  },
                  new string[] { }
                }
              });
            });
            
            services.AddHttpClient("report", client =>
            {
              client.BaseAddress = new Uri("10.10.100.105");
              client.DefaultRequestHeaders.Add("Accept", "application/json");
              client.Timeout = TimeSpan.FromSeconds(120);
            
            });

            var reportSp = ServicePointManager.FindServicePoint(new Uri("http://10.10.100.105"));
            reportSp.ConnectionLimit = 200;


            services.AddAuthentication("Zerpa")//JwtBearerDefaults.AuthenticationScheme)

              .AddJwtBearer("OpenID",options =>
              {
                options.Authority = Configuration["OpenID:authority"];
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                  ValidateIssuer = false,
                  ValidateAudience = false

                };
                //options.MetadataAddress = "http://localhost:[80]/.well-known/openid-configuration";

              })
              .AddJwtBearer("Zerpa", options =>
              {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                  ValidateIssuer = true,
                  ValidIssuer = Configuration["jwt_config:issuer"],
                  ValidateIssuerSigningKey = true,
                  IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["jwt_config:secret"])),
                  ValidAudience = Configuration["jwt_config:audience"],
                  ValidateAudience = true,
                  ValidateLifetime = true,
                  ClockSkew = TimeSpan.FromMinutes(1)
                };
              });
            services.AddAuthorization(options =>
            {
              options.DefaultPolicy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser()
                .AddAuthenticationSchemes("Zerpa", "OpenID").Build();
              options.AddPolicy("BILL-READ", policy =>
              {
                policy.RequireAuthenticatedUser();
                policy.AddAuthenticationSchemes("OpenID");
                policy.RequireClaim("scope", "bill.read");
              });
            });
          //   var setting = new Settings(){
            
          //     smtp_server = Configuration["Settings:SMTPServer"],
          //     smtp_user_name = Configuration["Settings:SMTPUserName"],
          //     smtp_password = Configuration["Settings:SMTPPassword"],
          //     smtp_port = int.Parse(Configuration["Settings:SMTPPort"]),
          //     smtp_use_ssl = bool.Parse(Configuration["Settings:SMTPUseSSL"])
          //   };
          //  services.AddSingleton<Settings>(setting);
            services.AddCors();
            // services.AddCronJob<SubjectService>(c =>
            // {
            //   c.TimeZoneInfo = TimeZoneInfo.Local;
            //   c.CronExpression = @"1 0 * * *";
            //   //c.CronExpression = @"*/5 * * * *";
            // });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
          IdentityModelEventSource.ShowPII = true;
            // if (env.IsDevelopment())
            // {
                app.UseDeveloperExceptionPage();
            //     app.UseSwagger();
            //     app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/swagger.json", "etest_test_api v1"));
            // }
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
              ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
              KnownNetworks =
              {
                new IPNetwork(IPAddress.Any, 0)
              }
            });
            // if (env.IsDevelopment())
            // {
              app.UseSwagger(c => {
                c.RouteTemplate = "swagger/{documentName}/swagger.json";
                c.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
                {
                  if (!httpReq.Headers.ContainsKey("X-Forwarded-Host")) return;

                  var serverUrl = $"{httpReq.Headers["X-Scheme"]}://" +
                                  $"{httpReq.Headers["X-Forwarded-Host"]}" +
                                  $"{httpReq.Headers["X-Forwarded-Prefix"]}";
                  swaggerDoc.Servers = new List<OpenApiServer> { new OpenApiServer { Url =serverUrl}};// $"{httpReq.Scheme}://{httpReq.Host.Value}{swaggerBasePath}" } };
                });
              });
              app.UseSwaggerUI(c =>
              {
                c.SwaggerEndpoint("subject/swagger.json", "Subject API v1");
                c.SwaggerEndpoint("test/swagger.json", "Test API v1");
                c.SwaggerEndpoint("utilities/swagger.json", "Utilities API v1");
                c.SwaggerEndpoint("user/swagger.json", "User API v1");
                c.SwaggerEndpoint("security/swagger.json", "Security API v1");
                c.DefaultModelExpandDepth(0);
                c.DefaultModelsExpandDepth(-1);
              });
          //  }
            InitializeDatabase(app);
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
          //  app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private void InitializeDatabase(IApplicationBuilder app)
        {
          using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
          var context = serviceScope.ServiceProvider.GetRequiredService<SeventyOneDevCoreContext>();
          var dbContext = serviceScope.ServiceProvider.GetRequiredService<Db>();
          var setting = serviceScope.ServiceProvider.GetRequiredService<IOptions<Setting>>();
          // if (!dbContext.t_system.Any())
          // {
          //   dbContext.AddRange( service_center_api.Initials.System.Get());
          //   dbContext.SaveChanges();
          // }
          // if (!dbContext.t_location_type.Any())
          // {
          //   dbContext.AddRange(LocationType.Get());
          //   dbContext.SaveChanges();
          // }
          CreateViews(context.Database,setting.Value.default_schema);


        }
        private static void CreateViews(DatabaseFacade db,string schema)
        {
          //InjectView(db, "app_data.sql", "v_app_data",schema);
          var assembly = Assembly.GetAssembly(typeof(SeventyOneDev.Core.ServiceCollectionExtension));
          string[] names = assembly.GetManifestResourceNames();
          foreach (var name in names)
          {
            var resource = assembly.GetManifestResourceStream(name);
            var sqlQuery = new StreamReader(resource).ReadToEnd();
            //we always delete the old view, in case the sql query has changed
            //db.ExecuteSqlRaw($"IF OBJECT_ID('{viewName}') IS NOT NULL BEGIN DROP VIEW {viewName} END");
            //creating a view based on the sql query
            db.ExecuteSqlRaw(string.Format(sqlQuery, schema));
          }
          //var assemblyName = assembly.FullName.Substring(0, assembly.FullName.IndexOf(','));

        }
    }
}
