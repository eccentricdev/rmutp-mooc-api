using Microsoft.Extensions.DependencyInjection;
using rmutp_mooc_api.Modules.Test.Services;
using rmutp_mooc_api.Modules.TestCertificate.Services;
using rmutp_mooc_api.Modules.TestDetail.Services;
using rmutp_mooc_api.Modules.TestSubject.Services;

namespace rmutp_mooc_api.Modules.Test.Configures
{
  public static class TestCollectionExtension
  {
    public static IServiceCollection AddTestServices(this IServiceCollection services)
    {
      //services.AddAutoMapper(typeof(ProfileMapping));
      services.AddScoped<TestService>();
      services.AddScoped<TestSubjectService>();
      services.AddScoped<TestCertificateService>();
      services.AddScoped<TestDetailService>();
      // services.AddScoped<DynamicTestEducationTypeService>();
      return services;
    }
  }
}
