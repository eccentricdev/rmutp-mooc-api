using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rmutp_mooc_api.Modules.TestDetail.Services;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;
using rmutp_mooc_api.Modules.Test.Databases.Models;

namespace rmutp_mooc_api.Modules.TestDetail.Controllers
{
  [Route("api/test/test_detail", Name = "test")]
  public class TestDetailController:Controller
  {
    private readonly Db _db;
    private readonly TestDetailService _entityUidService;
    public TestDetailController(TestDetailService entityUidService,Db db) 
    {
      _db = db;
      _entityUidService = entityUidService;
    }
    [HttpGet, Route("reset_test_detail/{test_detail_uid}")]
    public async Task<ActionResult<v_test_certificate>> GetWaitingList([FromRoute] Guid test_detail_uid)
    {
      return Ok(await _entityUidService.GetResetTestDetail(test_detail_uid));
    }
    
  }
}
