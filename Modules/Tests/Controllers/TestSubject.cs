using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rmutp_mooc_api.Modules.TestSubject.Services;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;
using rmutp_mooc_api.Modules.Test.Databases.Models;

namespace rmutp_mooc_api.Modules.TestSubject.Controllers
{
  [Route("api/test/test_subject", Name = "test")]
  public class TestSubjectController:Controller
  {
    private readonly Db _db;
    private readonly TestSubjectService _entityUidService;
    public TestSubjectController(TestSubjectService entityUidService,Db db) 
    {
      _db = db;
      _entityUidService = entityUidService;
    }
    [HttpGet, Route("undone_test_subject/{user_uid}")]
    public async Task<ActionResult<List<v_test_subject>>> GetUndoneList([FromRoute]Guid user_uid)
    {
      return Ok(await _entityUidService.GetUndoneTestSubject(user_uid));
    }
    [HttpGet, Route("reset/")]
    public async Task<ActionResult<List<v_test_subject>>> GetReset()
    {
      return Ok(await _entityUidService.ResetTestSubject());
    }
    
  }
}
