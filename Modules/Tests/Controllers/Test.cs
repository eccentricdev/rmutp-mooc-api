using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rmutp_mooc_api.Modules.Test.Services;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;
using rmutp_mooc_api.Modules.Test.Databases.Models;

namespace rmutp_mooc_api.Modules.Test.Controllers
{
  [Route("api/test/test", Name = "question")]
  public class TestController:Controller
  {
    private readonly Db _db;
    private readonly TestService _entityUidService;
    public TestController(TestService entityUidService,Db db) 
    {
      _db = db;
      _entityUidService = entityUidService;
    }
    [HttpGet, Route("information_subject/{subject_uid}/{user_uid}")]
    public async Task<ActionResult<test_student>> GetInformationSubject([FromRoute]Guid subject_uid,[FromRoute]Guid user_uid)
    {
      return Ok(await _entityUidService.GetInformationSubject(subject_uid,user_uid));
    }
    [HttpGet, Route("start_enroll/{subject_uid}/{user_uid}")]
    public async Task<ActionResult<t_test>> GetStartCourse([FromRoute]Guid subject_uid,[FromRoute]Guid user_uid)
    {
      return Ok(await _entityUidService.StartEnroll(subject_uid,user_uid));
    }
    [HttpGet, Route("start_test/{course_activity_uid}/{test_subject_uid}")]
    public async Task<ActionResult<test_student>> GetStartTestActivity([FromRoute]Guid course_activity_uid,[FromRoute]Guid test_subject_uid)
    {
      return Ok(await _entityUidService.StartTestActivity(course_activity_uid,test_subject_uid));
    }
    [HttpPost, Route("submit_test/{course_activity_uid}")]
    public async Task<ActionResult<test_student>> GetSubmitTestActivity([FromRoute]Guid course_activity_uid,[FromBody]test_student test_student)
    {
      return Ok(await _entityUidService.SubmitTestActivity(test_student,course_activity_uid));
    }
    // [HttpPost, Route("submit_lesson/{course_activity_uid}")]
    // public async Task<ActionResult<t_test_detail>> GetSubmitLessonActivity([FromRoute]Guid course_activity_uid,[FromBody]test_student test_student)
    // {
    //   return Ok(await _entityUidService.SubmitLessonActivity(test_student,course_activity_uid));
    // }
    [HttpGet, Route("list_check_written_test/")]
    public async Task<ActionResult<test_written>> GetListCheckWrittenTest()
    {
      return Ok(await _entityUidService.ListCheckWrittenTest());
    }
    [HttpGet, Route("check_written_test_detail/{test_subject_uid}")]
    public async Task<ActionResult<v_test_detail>> GetCheckWrittenTestDetail([FromRoute]Guid test_subject_uid)
    {
      return Ok(await _entityUidService.CheckWrittenTestDetail(test_subject_uid));
    }
    [HttpPost, Route("check_written_test/")]
    public async Task<ActionResult<test_written>> GetCheckWrittenTest([FromBody]test_written test_written)
    {
      return Ok(await _entityUidService.CheckWrittenTest(test_written));
    }
  }
}
