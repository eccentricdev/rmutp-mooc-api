using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rmutp_mooc_api.Modules.TestCertificate.Services;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;
using rmutp_mooc_api.Modules.Test.Databases.Models;

namespace rmutp_mooc_api.Modules.TestCertificate.Controllers
{
  [Route("api/test/test_certificate", Name = "test")]
  public class TestCertificateController:Controller
  {
    private readonly Db _db;
    private readonly TestCertificateService _entityUidService;
    public TestCertificateController(TestCertificateService entityUidService,Db db) 
    {
      _db = db;
      _entityUidService = entityUidService;
    }
    [HttpGet, Route("waiting_list/")]
    public async Task<ActionResult<v_test_certificate>> GetWaitingList([FromQuery] waiting_certificate waiting_Certificate)
    {
      return Ok(await _entityUidService.GetWaitingList(waiting_Certificate));
    }
    
  }
}
