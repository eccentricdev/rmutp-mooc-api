using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rmutp_mooc_api.Modules.Test.Databases.Models;
using SeventyOneDev.Utilities;

namespace rmutp_mooc_api.Modules.TestCertificate.Services
{
  public class TestCertificateService
  {
    private readonly Db _context;
    public TestCertificateService(Db context) 
    {
      _context = context;
    }

    public async Task<List<v_test_certificate>> GetWaitingList(waiting_certificate _waiting_Certificate)
    {
      IQueryable<v_test_certificate> result = _context.v_test_certificate.AsNoTracking().Where(c=>c.is_complete == null);
      if(_waiting_Certificate.subject_code!=null){
        result = result.Where(c=>c.subject_code == _waiting_Certificate.subject_code);
      }
      if(_waiting_Certificate.created_datetime!=null){
        result = result.Where(c=>c.created_datetime == _waiting_Certificate.created_datetime);
      }
      if(_waiting_Certificate.full_name!=null){
        result = result.Where(c=>c.full_name == _waiting_Certificate.full_name);
      }
      return await result.ToListAsync();
    }
  }
}
