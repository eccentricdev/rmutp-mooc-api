using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rmutp_mooc_api.Modules.Test.Databases.Models;
using SeventyOneDev.Utilities;

namespace rmutp_mooc_api.Modules.TestSubject.Services
{
  public class TestSubjectService
  {
    private readonly Db _context;
    public TestSubjectService(Db context) 
    {
      _context = context;
    }

    public async Task<List<v_test_subject>> GetUndoneTestSubject(Guid? user_uid)
    {
      return await _context.v_test_subject.Where(c=>c.is_complete == null && c.user_uid ==user_uid).ToListAsync();
    }
    public async Task<List<v_test_subject>> ResetTestSubject()
    {
      var test_subjects = _context.t_test_subject.AsNoTracking().ToList();
      foreach(var test_subject in test_subjects){
        // var entity = await _context.t_test_subject.GetByKey(test_subject.test_uid);
        _context.t_test_subject.Remove(test_subject);
        await _context.SaveChangesAsync();
      }
      return _context.v_test_subject.AsNoTracking().ToList();
    }
  }
}
