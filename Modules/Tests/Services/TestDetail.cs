using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rmutp_mooc_api.Modules.Test.Databases.Models;
using SeventyOneDev.Utilities;

namespace rmutp_mooc_api.Modules.TestDetail.Services
{
  public class TestDetailService
  {
    private readonly Db _context;
    public TestDetailService(Db context) 
    {
      _context = context;
    }
    public async Task<t_test_detail> GetResetTestDetail(Guid test_detail_uid)
    {
        var test_detail = await _context.t_test_detail.FirstOrDefaultAsync(c=>c.test_detail_uid == test_detail_uid);
        if(test_detail!=null){
            var test = await _context.t_test.FirstOrDefaultAsync(c=>c.test_uid == test_detail.test_uid);
            if(test!=null){
                test.remark = null;
                test.is_complete = null;
                test.is_pass = null;
                _context.t_test.Update(test);
            }
            test_detail.is_complete =null;
            test_detail.is_pass =null;
            test_detail.question_corrent_percent =null;
            test_detail.total_reset++;
            _context.t_test_detail.Update(test_detail);
            await _context.SaveChangesAsync();
        }
        return test_detail;
    }

  }
}
