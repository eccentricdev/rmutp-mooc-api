using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rmutp_mooc_api.Modules.Test.Databases.Models;
using SeventyOneDev.Utilities;

namespace rmutp_mooc_api.Modules.Test.Services
{
  public class TestService
  {
    private readonly Db _context;
    public TestService(Db context) 
    {
      _context = context;
    }

    public async Task<test_student> GetInformationSubject(Guid? subject_uid,Guid? user_uid )
    {
      var test_student = new test_student(){
        test_subject_information = await this.GetTestInformationSubject(subject_uid,user_uid)
      };
      var test_subject =await _context.v_test_subject.FirstOrDefaultAsync(c=>c.subject_uid==subject_uid && c.user_uid == user_uid && c.is_complete == null);
      if(test_subject!=null){
        var test = await _context.v_test.FirstOrDefaultAsync(c=>c.test_subject_uid==test_subject.test_subject_uid && c.is_complete == null);
        if(test!=null){
          test_subject.tests = null;
          test_student.test_subject = test_subject;
        }
      }
      return test_student;
    }
    public async Task<test_subject_information> GetTestInformationSubject(Guid? subject_uid,Guid? user_uid )
    {
      var subject = await _context.v_subject.FirstOrDefaultAsync(c=>c.subject_uid == subject_uid);
      var test_subject = await _context.v_test_subject.FirstOrDefaultAsync(c=>c.subject_uid == subject_uid && c.is_complete==null && c.user_uid==user_uid);
      var test_subject_information =new test_subject_information();
      if(test_subject!=null){
        var test_informations = new List<test_information>();
        var courses = await _context.v_course.Where(c=>c.subject_uid == subject_uid).OrderBy(c=>c.row_no).ToListAsync();
        foreach(var course in courses){
          var test_information = new test_information(){
            course_uid = course.course_uid,
            course_name = course.course_name_th,
          };
          var course_activitys = await _context.v_course_activity.Where(c=>c.course_uid==course.course_uid).OrderBy(c=>c.row_no).ToListAsync();
          var test = await _context.v_test.FirstOrDefaultAsync(c=>c.test_subject_uid == test_subject.test_subject_uid && c.course_uid == course.course_uid);
          if(test!=null){
            test_information.test_uid = test.test_uid;
            test_information.is_complete = test.is_complete;
            var test_detail_informations = new List<test_detail_information>();
            foreach(var course_activity in course_activitys){
              var test_detail = await _context.v_test_detail.FirstOrDefaultAsync(c=>c.test_uid == test.test_uid && c.course_activity_uid == course_activity.course_activity_uid);
              if(test_detail!=null){
                var activity = await _context.v_activity.FirstOrDefaultAsync(c=>c.activity_uid==course_activity.activity_uid);
                test_detail_informations.Add(new test_detail_information(){
                  test_time = course_activity.test_time,
                  test_question_number = course_activity.test_question_number,
                  course_activity_uid = course_activity.course_activity_uid,
                  activity_code = activity.activity_code,
                  activity_name_th = activity.activity_name_th,
                  course_activity_name = course_activity.course_activity_name_th,
                  is_complete = test_detail.is_complete,
                });
              }
            }
            
            test_information.test_detail_informations=test_detail_informations;
            test_informations.Add(test_information);
          }
        }
        test_subject_information.subject_name =subject.subject_name_th;
        test_subject_information.test_informations =test_informations;
      }
      return test_subject_information;
    }
    public async Task<t_test_subject> StartEnroll(Guid subject_uid,Guid user_uid)
    {
      Nullable<int> test_no = 1;
      var tests_number = await _context.v_test_subject.Where(c=>c.subject_uid == subject_uid && c.user_uid == user_uid).ToListAsync();
      if(tests_number.Count()>0){
        test_no += tests_number[0]?.test_subject_no;
      }
      var userCheck = await _context.v_test_subject.FirstOrDefaultAsync(c=>c.user_uid == user_uid && c.subject_uid == subject_uid && c.is_complete == null);
      var tests = new List<t_test>();
      var test_subject_uid = Guid.NewGuid();
      if(userCheck == null ){
        var subject = await _context.v_subject.Include(c=>c.courses).ThenInclude(c=>c.course_activitys).FirstOrDefaultAsync(c=>c.subject_uid == subject_uid);
        var courseCount = await _context.v_course.Where(c=>c.subject_uid == subject_uid).OrderBy(c=>c.row_no).ToListAsync();
        foreach(var course in subject.courses){
          tests.Add(await this.StartCourse(course.course_uid,user_uid,test_subject_uid));
        }
        
        await _context.t_test_subject.AddAsync(new t_test_subject(){
          subject_uid = subject_uid,
          test_subject_uid = test_subject_uid,
          user_uid = user_uid,
          test_subject_no = test_no,
          test_subject_code = "C"+test_no,
          start_test_datetime = DateTime.Now,
          is_complete = null,
          tests = tests,
        });
        await _context.SaveChangesAsync();
      }
      return await _context.t_test_subject.FirstOrDefaultAsync(c=>c.test_subject_uid==test_subject_uid);
    }
    public async Task<t_test> StartCourse(Guid? course_uid,Guid user_uid,Guid test_subject_uid)
    {
      var test_uid = Guid.NewGuid();
      var course = await _context.v_course.FirstOrDefaultAsync(c=>c.course_uid == course_uid);   
      var course_activitys = await _context.v_course_activity.Where(c=>c.course_uid == course_uid).OrderBy(c=>c.row_no).ToListAsync();   
      var test_details = new List<t_test_detail>();
      foreach(var course_activity in course_activitys){
        var test_detail_uid = Guid.NewGuid();
        var random_uid = Guid.NewGuid();
        var test_questions = new List<t_test_question>();
        var activity = await _context.v_activity.FirstOrDefaultAsync(c=>c.activity_uid == course_activity.activity_uid);     
        var questions = await _context.v_question.Where(c=>c.course_activity_uid == course_activity.course_activity_uid)
          .OrderBy(r => random_uid).Take(course_activity.test_question_number).ToListAsync();
        var test_detail = new t_test_detail(){
          course_activity_uid = course_activity.course_activity_uid,
          test_uid = test_uid,
          is_complete = null,
          test_detail_uid = test_detail_uid
        };
        foreach(var question in questions){
          test_questions.Add(new t_test_question(){
            test_question_uid = Guid.NewGuid(),
            test_detail_uid = test_detail_uid,
            question_uid = question.question_uid,
            test_question_answer = "",
          });
        }
        if(test_questions.Count()>0){
          test_detail.test_questions = test_questions;
        }
        
        test_details.Add(test_detail);
      }
      return new t_test(){
        course_uid = course_uid,
        test_subject_uid = test_subject_uid,
        is_complete = null,
        test_uid = test_uid,
        test_details = test_details,
      };
    }
    
    public async Task<test_student> StartTestActivity(Guid course_activity_uid,Guid test_subject_uid)
    {
      Guid? test_uid = Guid.Empty;
      var test_subject = await _context.v_test_subject.Include(c=>c.tests).ThenInclude(c=>c.test_details).FirstOrDefaultAsync(c=>c.test_subject_uid == test_subject_uid);
      foreach(var test in test_subject.tests){
        foreach(var test_de in test.test_details){
          if(test_de.course_activity_uid == course_activity_uid){
            test_uid = test_de.test_uid;
          }
        }
      }
      var course_activity = await _context.v_course_activity.FirstOrDefaultAsync(c=>c.course_activity_uid == course_activity_uid);
      var activity = await _context.v_activity.FirstOrDefaultAsync(c=>c.activity_uid == course_activity.activity_uid);
      var test_detail = await _context.v_test_detail.Include(c=>c.test_questions).FirstOrDefaultAsync(c=>c.test_uid == test_uid && c.course_activity_uid == course_activity_uid);
      var questions = new List<question>();
      foreach(var qt in test_detail.test_questions){
        var qt_detail = await _context.v_question.Include(c=>c.choices).FirstOrDefaultAsync(c=>c.question_uid == qt.question_uid);
        var choices = new List<choice>();
        foreach(var ch in qt_detail.choices){
          choices.Add(new choice(){
            choice_uid = ch.choice_uid,
            choice_name = ch.choice_name,
          });
        }
        
        var test_question = await _context.v_test_question.FirstOrDefaultAsync(c=>c.test_detail_uid == test_detail.test_detail_uid && c.question_uid == qt.question_uid);
        questions.Add(new question(){
          question_name = qt_detail.question_name,
          question_uid = qt_detail.question_uid,
          choices = choices,
          test_question_answer = test_question.test_question_answer,
          selected_choice_uid = test_question.selected_choice_uid,
        });
      }
      test_subject.tests=null;
      var startTest = new test_student(){
        test_subject_information = await this.GetTestInformationSubject(test_subject.subject_uid,test_subject.user_uid),
        test_subject = test_subject,
        questions = questions,
      };
      if(activity.activity_code == "lesson"){
        startTest.source_learning_video = course_activity.source_learning_video;
      }
      return startTest;
    }
    // public async Task<t_test_detail> SubmitLessonActivity(test_student test_student,Guid course_activity_uid)
    // {
    //   Guid? test_uid = Guid.Empty;
    //   var test_subject = await _context.v_test_subject.Include(c=>c.tests).ThenInclude(c=>c.test_details).FirstOrDefaultAsync(c=>c.test_subject_uid == test_student.test_subject.test_subject_uid);
    //   foreach(var test in test_subject.tests){
    //     foreach(var test_de in test.test_details){
    //       if(test_de.course_activity_uid == course_activity_uid){
    //         test_uid = test_de.test_uid;
    //       }
    //     }
    //   }
    //   var test_detail = await _context.t_test_detail.FirstOrDefaultAsync(c=>c.course_activity_uid == course_activity_uid && c.test_uid == test_uid);
    //   test_detail.is_complete = true;
    //   _context.t_test_detail.Update(test_detail);
    //   await _context.SaveChangesAsync();
    //   var test_detail_recheck = await _context.v_test_detail.FirstOrDefaultAsync(c=>c.test_uid==test_uid && c.is_complete == null);
    //   if(test_detail_recheck==null){
    //     var test = await _context.t_test.FirstOrDefaultAsync(c=>c.test_uid == test_uid);
    //     test.is_complete =true;
    //     _context.t_test.Update(test);
    //     await _context.SaveChangesAsync();
    //     var test_check = await _context.t_test.FirstOrDefaultAsync(c=>c.test_subject_uid == test.test_subject_uid && c.is_complete == null);
    //     if(test_check == null ){
    //       var test_subject_table = await _context.t_test_subject.FirstOrDefaultAsync(c=>c.test_subject_uid == test.test_subject_uid);
    //       _context.t_test_subject.Update(test_subject_table);
    //       await _context.SaveChangesAsync();
    //     }
    //   }
    //   return test_detail;
    // }
    public async Task<test_student> SubmitTestActivity(test_student test_student,Guid course_activity_uid)
    {
      Guid? test_uid = Guid.Empty;
      var test_subject = await _context.v_test_subject.Include(c=>c.tests).ThenInclude(c=>c.test_details).FirstOrDefaultAsync(c=>c.test_subject_uid == test_student.test_subject.test_subject_uid);
      foreach(var test in test_subject.tests){
        foreach(var test_de in test.test_details){
          if(test_de.course_activity_uid == course_activity_uid){
            test_uid = test_de.test_uid;
          }
        }
      }
      var test_detail = await _context.t_test_detail.FirstOrDefaultAsync(c=>c.test_uid == test_uid && c.course_activity_uid == course_activity_uid);
      var correct_number = 0;
      var no_empty = true;
      foreach(var question in test_student.questions){
        var test_question = await _context.t_test_question.FirstOrDefaultAsync(c=>c.test_detail_uid==test_detail.test_detail_uid);
        var question_detail = await _context.v_question.FirstOrDefaultAsync(c=>c.question_uid == question.question_uid);
        if(question_detail!=null){
          if(question.selected_choice_uid == question_detail.correct_choice_uid){
            correct_number++;
          }
        }
        if(question.selected_choice_uid == null){
          no_empty = false;
        }
        test_question.selected_choice_uid = question.selected_choice_uid;
        test_question.test_question_answer = question.test_question_answer;
        _context.t_test_question.Update(test_question);
        await _context.SaveChangesAsync();
      }
      var subject = await _context.v_subject.FirstOrDefaultAsync(c => c.subject_uid == test_subject.subject_uid);
      var course_activity = await _context.v_course_activity.FirstOrDefaultAsync(c=>c.course_activity_uid == course_activity_uid);
      if(course_activity!=null){
        test_detail.is_pass = false;
        var activity = await _context.v_activity.FirstOrDefaultAsync(c=>c.activity_uid == course_activity.activity_uid);
        if(activity!=null){
          switch(activity.activity_code){
            case "Quiz":
              test_detail.is_pass = null;
              break;
            case "lesson":
              test_detail.is_pass = true;
              break;
            default :
              if(no_empty == true){
                if(activity.activity_code == "pre-test"){
                  test_detail.is_pass = true;
                }
                // if(correct_number >0){
                test_detail.question_corrent_percent = (correct_number/course_activity.test_question_number)*100;
                if(test_detail.question_corrent_percent >= course_activity.minimum_score_percent){
                  test_detail.is_pass = true;
                }
                // }
              }
              break;
          }
          if(subject != null){
            if(subject.is_certificate_reward != true){
              test_detail.is_pass = true;
            }
          }
          // if(activity.activity_code != "Quiz" && activity.activity_code != "lesson"){
            
          // }else{
          //   test_detail.is_pass = true;
          // }
        }
      }
      test_detail.is_complete = true;
      _context.t_test_detail.Update(test_detail);
      await _context.SaveChangesAsync();
      var test_detail_recheck = await _context.v_test_detail.FirstOrDefaultAsync(c=>c.test_uid==test_uid && c.is_complete == null);
      if(test_detail_recheck==null){
        var test = await _context.t_test.FirstOrDefaultAsync(c=>c.test_uid == test_uid);
        if(subject != null){
          if(subject.is_certificate_reward != true){
            test.is_pass = true;
          }
        }
        test.is_complete =true;
        _context.t_test.Update(test);
        await _context.SaveChangesAsync();
        var test_check = await _context.t_test.FirstOrDefaultAsync(c=>c.test_subject_uid == test.test_subject_uid && c.is_complete == null);
        if(test_check == null ){
          var test_subject_table = await _context.t_test_subject.FirstOrDefaultAsync(c=>c.test_subject_uid == test.test_subject_uid);
          test_subject_table.is_complete = true;
          test_subject_table.end_test_datetime = DateTime.Now;
          _context.t_test_subject.Update(test_subject_table);
          await _context.SaveChangesAsync();
        }
      }
      test_student.test_subject_information = await this.GetTestInformationSubject(test_subject.subject_uid,test_subject.user_uid);
      test_student.is_pass = test_detail.is_pass;
      return test_student;
    }
    public async Task<test_written> CheckWrittenTest(test_written test_written)
    {
      var test = await _context.t_test.FirstOrDefaultAsync(c=>c.test_uid == test_written.test_uid);
      test.is_pass = test_written.is_pass;
      if(test_written.remark!=null){
        test.remark = test_written.remark;
      }
      _context.t_test.Update(test);
      await _context.SaveChangesAsync();
      return test_written;
    }
    public async Task<List<v_test>> ListCheckWrittenTest()
    {
      var activity = await _context.v_activity.FirstOrDefaultAsync(c=>c.activity_code == "Quiz");
      var course_activitys = await _context.v_course_activity.Where(c=>c.activity_uid == activity.activity_uid).ToListAsync();
      var tests = new List<v_test>();
      foreach(var course_activity in course_activitys){
        var test_detail = await _context.v_test_detail.FirstOrDefaultAsync(c=>c.course_activity_uid == course_activity.course_activity_uid && c.is_pass == null);
        if(test_detail!=null){          
          var test = await _context.v_test.FirstOrDefaultAsync(c=>c.test_uid == test_detail.test_uid);
          test.test_details=null;
          tests.Add(test);
        }
      }
      return tests;
    }
    public async Task<List<v_test_detail>> CheckWrittenTestDetail(Guid test_subject_uid)
    {
      var activity = await _context.v_activity.FirstOrDefaultAsync(c=>c.activity_code == "Quiz");
      var test_details = new List<v_test_detail>();
      var test_subject = await _context.v_test_subject.Include(c=>c.tests).ThenInclude(c=>c.test_details).FirstOrDefaultAsync(c=>c.test_subject_uid == test_subject_uid);
      foreach(var test in test_subject.tests){
        foreach(var test_detail in test.test_details){
          test_detail.test=null;
           var course_activity = await _context.v_course_activity.FirstOrDefaultAsync(c=>c.activity_uid == activity.activity_uid && c.course_uid == test.course_uid );
           if(course_activity!=null){
             if(course_activity.course_activity_uid == test_detail.course_activity_uid){
              test_details.Add(test_detail);
             }
           }
        }
      }
      return test_details;
    }
  }
}
