using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Test.Databases.Models
{
  public class test_written
  {
    public Guid? test_uid { get; set; }
    public string remark { get; set; }
    public Boolean? is_pass { get; set; }
  }
}
