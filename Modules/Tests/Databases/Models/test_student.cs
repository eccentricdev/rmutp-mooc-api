using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Test.Databases.Models
{
  public class test_student
  {
    // public v_subject subject { get; set; }
    public test_subject_information test_subject_information { get; set; }
    public v_test_subject test_subject { get; set; }
    // public v_course_activity course_activity { get; set; }
    // public Guid test_detail_guid { get; set; }
    public List<question> questions { get; set; }
    public Boolean? is_pass { get; set; }
    public string source_learning_video { get; set; }
  }
  public class question
  {
    public Guid? question_uid { get; set; }
    public string question_name { get; set; }
    public Guid? selected_choice_uid { get; set; }
    public string test_question_answer { get; set; }
    public List<choice> choices { get; set; }
  }
  public class choice
  {
    public Guid? choice_uid { get; set; }
    public string choice_name { get; set; }
  }
  public class test_subject_information
  {
    public string subject_name { get; set; }
    public Boolean? is_complete { get; set; }
    public List<test_information> test_informations { get; set; }
  }
  public class test_information
  {
    public Guid? test_uid { get; set; }
    public Guid? course_uid { get; set; }
    public string course_name { get; set; }
    public Boolean? is_complete { get; set; }
    public List<test_detail_information> test_detail_informations { get; set; }
  }
  public class test_detail_information
  {
    public Guid? course_activity_uid { get; set; }
    public int test_question_number { get; set; }
    public int? test_time { get; set; }
    public string course_activity_name { get; set; }
    public string activity_name_th { get; set; }
    public string activity_code { get; set; }
    public Boolean? is_complete { get; set; }
  }
}
