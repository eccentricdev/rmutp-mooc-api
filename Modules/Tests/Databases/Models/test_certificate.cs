using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Test.Databases.Models
{
  public class test_certificate:base_table
  {
    [Key] public Guid? test_certificate_uid { get; set; }
    public Guid? test_subject_uid { get; set; }
    public Guid? user_uid { get; set; }
    public Boolean? is_agree { get; set; }
    public Boolean? is_complete { get; set; }
    public string citizen_no { get; set; }
    public string image { get; set; }
  }
  [GeneratedUidController("api/test/test_certificate")]
  public class t_test_certificate : test_certificate
  {
  }

  public class v_test_certificate : test_certificate
  {
    public string subject_code { get; set; }
    public string full_name { get; set; }
  }
  public class waiting_certificate
  {
    public DateTime? created_datetime { get; set; }
    public string subject_code { get; set; }
    public string full_name { get; set; }
  }
}
