using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Test.Databases.Models
{
  public class test_detail:base_table
  {
    [Key] public Guid? test_detail_uid { get; set; }
    public Guid? test_uid { get; set; }
    public Guid? course_activity_uid { get; set; }
    public Boolean? is_complete { get; set; }
    public Boolean? is_pass { get; set; }
    public int total_reset { get; set; }
    public int? question_corrent_percent { get; set; }
  }
  [GeneratedUidController("api/test/test_detail")]
  public class t_test_detail : test_detail
  {
    [JsonIgnore]
    public t_test test { get; set; }
    [Include]
    public List<t_test_question> test_questions { get; set; }
  }

  public class v_test_detail : test_detail
  {
    [JsonIgnore]
    public v_test test { get; set; }
    [Include]
    public List<v_test_question> test_questions { get; set; }
  }
}
