using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Test.Databases.Models
{
  public class test:base_table
  {
    [Key] public Guid? test_uid { get; set; }
    public Guid? test_subject_uid { get; set; }
    // public Guid? user_uid { get; set; }
    public Guid? course_uid { get; set; }
    // public int? test_no { get; set; }
    // public string test_code { get; set; }
    public string remark { get; set; }
    public Boolean? is_complete { get; set; }
    public Boolean? is_pass { get; set; }
  }
  [GeneratedUidController("api/test/test")]
  public class t_test : test
  {
    [JsonIgnore]
    public t_test_subject test_subject { get; set; }
    [Include]
    public List<t_test_detail> test_details { get; set; }
  }

  public class v_test : test
  {
    [JsonIgnore]
    public v_test_subject test_subject { get; set; }
    [Include]
    public List<v_test_detail> test_details { get; set; }
  }
}
