using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Test.Databases.Models
{
  public class test_question:base_table
  {
    [Key] public Guid? test_question_uid { get; set; }
    public Guid? test_detail_uid { get; set; }
    public Guid? question_uid { get; set; }
    public Guid? selected_choice_uid { get; set; }
    public string test_question_answer { get; set; }
  }
  [GeneratedUidController("api/test/test_question")]
  public class t_test_question : test_question
  {
    [JsonIgnore]
    public t_test_detail test_detail { get; set; }
  }

  public class v_test_question : test_question
  {
    [JsonIgnore]
    public v_test_detail test_detail { get; set; }
  }
}
