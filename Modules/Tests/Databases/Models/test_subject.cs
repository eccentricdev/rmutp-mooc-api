using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Test.Databases.Models
{
  public class test_subject:base_table
  {
    [Key] public Guid? test_subject_uid { get; set; }
    public Guid? user_uid { get; set; }
    public Guid? subject_uid { get; set; }
    public int? test_subject_no { get; set; }
    public string test_subject_code { get; set; }
    public string remark { get; set; }
    public Boolean? is_complete { get; set; }
    public DateTime? start_test_datetime { get; set; }
    public DateTime? end_test_datetime { get; set; }
  }
  [GeneratedUidController("api/test/test_subject")]
  public class t_test_subject : test_subject
  {
    [Include]
    public List<t_test> tests { get; set; }
  }

  public class v_test_subject : test_subject
  {
    public string full_name { get; set; }
    public string phone_number { get; set; }
    public string faculty_name { get; set; }
    public string subject_image { get; set; }
    public string subject_name_th { get; set; }
    [Include]
    public List<v_test> tests { get; set; }
  }
}
