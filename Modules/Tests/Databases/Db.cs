using rmutp_mooc_api.Modules.Test.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_test> t_test {get;set;}
    public DbSet<v_test> v_test {get;set;} 
    public DbSet<t_test_subject> t_test_subject {get;set;}
    public DbSet<v_test_subject> v_test_subject {get;set;} 
    public DbSet<t_test_detail> t_test_detail {get;set;}
    public DbSet<v_test_detail> v_test_detail {get;set;} 
    public DbSet<t_test_question> t_test_question {get;set;}
    public DbSet<v_test_question> v_test_question {get;set;} 
    public DbSet<t_test_certificate> t_test_certificate {get;set;}
    public DbSet<v_test_certificate> v_test_certificate {get;set;} 
    private void OnTestModelCreating(ModelBuilder builder, string schema)
    {

      builder.Entity<t_test_subject>().ToTable("test_subject", schema);
      builder.Entity<v_test_subject>().ToView("v_test_subject", schema);
      
      builder.Entity<t_test>().ToTable("test", schema);
      builder.Entity<v_test>().ToView("v_test", schema);
      builder.Entity<t_test>().HasOne(p => p.test_subject).WithMany(p => p.tests).HasForeignKey(p => p.test_subject_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_test>().HasOne(p => p.test_subject).WithMany(p => p.tests).HasForeignKey(p => p.test_subject_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_test_detail>().ToTable("test_detail", schema);
      builder.Entity<v_test_detail>().ToView("v_test_detail", schema);
      builder.Entity<t_test_detail>().HasOne(p => p.test).WithMany(p => p.test_details).HasForeignKey(p => p.test_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_test_detail>().HasOne(p => p.test).WithMany(p => p.test_details).HasForeignKey(p => p.test_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_test_question>().ToTable("test_question", schema);
      builder.Entity<v_test_question>().ToView("v_test_question", schema);
      builder.Entity<t_test_question>().HasOne(p => p.test_detail).WithMany(p => p.test_questions).HasForeignKey(p => p.test_detail_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_test_question>().HasOne(p => p.test_detail).WithMany(p => p.test_questions).HasForeignKey(p => p.test_detail_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_test_certificate>().ToTable("test_certificate", schema);
      builder.Entity<v_test_certificate>().ToView("v_test_certificate", schema);
      
    }
  }
}
