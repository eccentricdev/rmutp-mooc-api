using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using rmutp_mooc_api.Modules.User.Databases.Models;
using rmutp_mooc_api.jwtConfig;
using SeventyOneDev.Utilities;
using rmutp_mooc_api.Modules.User.Interfaces;

namespace rmutp_mooc_api.Modules.User.Services
{
  public partial class UserService :ISecurity
  {
    private readonly Db _context;
    private readonly jwt_Config _jwtConfig;
    public UserService(jwt_Config jwtConfig, Db context) 
    {
      this._jwtConfig = jwtConfig;
      this._context = context;
    }

    // public async Task<test_student> GetInformationSubject(Guid? subject_uid)
    // {
    //   var subject = await __context.v_subject.Include(c=>c.courses).ThenInclude(c=>c.course_activitys).FirstOrDefaultAsync(c=>c.subject_uid == subject_uid);
    //   return new test_student(){
    //     subject = subject
    //   };
    // }
    // public IQueryable<v_user> ListUsers(v_user user)
    // {
        
    //     IQueryable<v_user> users= _context.v_user.AsNoTracking();
        
    //     if (user!=null){
    //         users = users.Where(m=>
    //             m.username.Contains(user.search) ||
    //             m.email.Contains(user.search)
    //         );
    //     }
    //     // users=users.OrderBy(c=>c.user_id);
    //     return users;
    // }
    public async Task<v_user> GetUser(Guid user_uid)
    {
        // var user = _context.v_user.FirstOrDefault(r=>r.user_uid==user_uid);
        // var time = user.date_of_birth.ToString().Split("T");
        // var newTime = time[0].Split("-");
        // user.date_of_birth_string = user.date_of_birth?.ToString("yyyy-MM-dd");
        return await _context.v_user.FirstOrDefaultAsync(r=>r.user_uid==user_uid);
        
    }
    public async Task<t_user> NewUser(t_user user)
    {
        // Task<register> resultRegis = null;
        if(user.password!=null){
            user.password = this.SHA512(user.password);
        }
        user.user_uid = Guid.NewGuid();
        await _context.t_user.AddAsync(user);
        await _context.SaveChangesAsync();
        // var userCheck = _context.v_user.FirstOrDefault(c =>c.user_id == user.user_id);
        // if(user.user_id!=null){
        //     resultRegis = this.Register(user);
        // }
        return user;
    }
    public string SHA512(string input)
    {
        var bytes = System.Text.Encoding.UTF8.GetBytes(input);
        using (var hash = System.Security.Cryptography.SHA512.Create())
        {
            var hashedInputBytes = hash.ComputeHash(bytes);

            // Convert to text
            // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
            var hashedInputStringBuilder = new System.Text.StringBuilder(512);
            foreach (var b in hashedInputBytes)
                hashedInputStringBuilder.Append(b.ToString("X2"));
            return hashedInputStringBuilder.ToString();
        }
    }

    // public void UpdateUser(t_user user,ClaimsIdentity claimsIdentity)
    // {
    //     _context.t_user.Update(user);
    //     _context.SaveChanges();
    // }

    // public int DeleteUser(Guid user_uid,ClaimsIdentity claimsIdentity)
    // {
    //     _context.t_user.Remove(new t_user(){user_uid=user_uid});
    //     return _context.SaveChanges();
        
    // }

    // public async Task<v_user> UserLogin(user_login user)
    // {
    //     return await _context.v_user.FirstOrDefaultAsync(r=>r.username==user.username || r.email==user.username);
    // }
    public string GenerateJWTToken(user_login userInfo)
    {
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfig.secret_key));
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
        var claims = new Claim[]
        {
            // new Claim(JwtRegisteredClaimNames.Sub, userInfo.email),
            new Claim("email", userInfo.email),
            new Claim("full_name",  userInfo.full_name),
            // new Claim("role",userInfo.UserRole),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        };
        var token = new JwtSecurityToken(
            issuer: _jwtConfig.issuer,
            audience: _jwtConfig.audience,
            claims: claims,
            expires: DateTime.Now.AddMinutes(120),
            signingCredentials: credentials
        );
        return new JwtSecurityTokenHandler().WriteToken(token);
        // return "7777";
    }
    public user_login Authenticate(user_login loginCredentials)
    {
        loginCredentials.password = this.SHA512(loginCredentials.password);
        user_login user = _context.v_user.FirstOrDefault(x => 
            x.email == loginCredentials.email && 
            x.password == loginCredentials.password);
        return user;
    }
  }
}
