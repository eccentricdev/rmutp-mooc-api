using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rmutp_mooc_api.Modules.User.Services;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;
using rmutp_mooc_api.Modules.User.Databases.Models;

namespace rmutp_mooc_api.Modules.User.Controllers
{
  [Route("api/user/user", Name = "user")]
  public class UserController:Controller
  {
    private readonly Db _db;
    private readonly UserService _entityUidService;
    public UserController(UserService entityUidService,Db db) 
    {
      _db = db;
      _entityUidService = entityUidService;
    }
    [HttpPost, Route("register")]
    public async Task<ActionResult<t_user>> GetInformationSubject([FromBody]t_user new_user)
    {
      return Ok(await _entityUidService.NewUser(new_user));
    }
    [HttpPost, Route("login")]
    public IActionResult LoginAdmin([FromBody]user_login login)
    {
      IActionResult response = Unauthorized();
      user_login user = _entityUidService.Authenticate(login);
      if (user != null)
      {
        var tokenString = _entityUidService.GenerateJWTToken(user);
        response = Ok(new
            {
            token = tokenString,
            userDetails = user,
            });
      }
      return response;
    }
  }
}
