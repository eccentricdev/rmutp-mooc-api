using Microsoft.Extensions.DependencyInjection;
using rmutp_mooc_api.Modules.User.Interfaces;
using rmutp_mooc_api.Modules.User.Services;

namespace rmutp_mooc_api.Modules.User.Configures
{
  public static class UserCollectionExtension
  {
    public static IServiceCollection AddUserServices(this IServiceCollection services)
    {
      //services.AddAutoMapper(typeof(ProfileMapping));
      services.AddScoped<UserService>();
      services.AddScoped<ISecurity,UserService>(); 
      // services.AddScoped<DynamicUserEducationTypeService>();
      return services;
    }
  }
}
