using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rmutp_mooc_api.Modules.User.Databases.Models
{
    
    public class user :user_login
    {
        [Key] public Guid user_uid { get; set; }
        // public Boolean? is_women { get; set; }
        public Boolean? is_student { get; set; }
        public string image { get; set; }
        public string phone_number { get; set; }
        public string agency_name { get; set; }
        public string role_name { get; set; }
        // public string citizen_no { get; set; }
        public string faculty_name { get; set; }
        public string major_name { get; set; }
        // public string citizen_card_front_image { get; set; }
        // public string citizen_card_back_image { get; set; }
        // public string driver_card_front_image { get; set; }
        // public string driver_card_back_image { get; set; }
        // public string verify_image { get; set; }
        // public int? age { get; set; }
        // public string user_job { get; set; }
        //  [Column(TypeName="decimal(18,2)")]
        // public decimal? income_per_month { get; set; }
        // [DataType(DataType.Date)]
        public DateTime? date_of_birth { get; set; }

    }
    public class user_login 
    {
        
        // public string username { get; set; }
        public string email { get; set; }
        public string full_name { get; set; }
        public string password { get; set; }

    }
    public class t_user : user 
    {
        
    }

    //View
    public class v_user : user
    {
        
        [NotMapped]
        public string search { get; set; }
    }
    
}