namespace rmutp_mooc_api.jwtConfig
{
    public class jwt_Config
    {
        public string issuer {get;set;}
        public string audience {get;set;}
        public string secret_key {get;set;}
    }
}