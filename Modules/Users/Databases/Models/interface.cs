using rmutp_mooc_api.Modules.User.Databases.Models;

namespace rmutp_mooc_api.Modules.User.Interfaces
{
    public partial interface ISecurity
    {
        user_login Authenticate(user_login loginCredentials);
        string GenerateJWTToken(user_login userInfo);
    }
}