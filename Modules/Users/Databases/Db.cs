using rmutp_mooc_api.Modules.Test.Databases.Models;
using Microsoft.EntityFrameworkCore;
using rmutp_mooc_api.Modules.User.Databases.Models;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_user> t_user {get;set;}
    public DbSet<v_user> v_user {get;set;} 
    // public DbSet<t_test_detail> t_test_detail {get;set;}
    // public DbSet<v_test_detail> v_test_detail {get;set;} 
    // public DbSet<t_test_question> t_test_question {get;set;}
    // public DbSet<v_test_question> v_test_question {get;set;} 
    private void OnUserModelCreating(ModelBuilder builder, string schema)
    {


      builder.Entity<t_user>().ToTable("user", schema);
      builder.Entity<v_user>().ToView("v_user", schema);

      // builder.Entity<t_test_detail>().ToTable("test_detail", schema);
      // builder.Entity<v_test_detail>().ToView("v_test_detail", schema);
      // builder.Entity<t_test_detail>().HasOne(p => p.test).WithMany(p => p.test_details).HasForeignKey(p => p.test_uid)
      //   .OnDelete(DeleteBehavior.Cascade);
      // builder.Entity<v_test_detail>().HasOne(p => p.test).WithMany(p => p.test_details).HasForeignKey(p => p.test_uid)
      //   .OnDelete(DeleteBehavior.Cascade);

      // builder.Entity<t_test_question>().ToTable("test_question", schema);
      // builder.Entity<v_test_question>().ToView("v_test_question", schema);
      // builder.Entity<t_test_question>().HasOne(p => p.test_detail).WithMany(p => p.test_questions).HasForeignKey(p => p.test_detail_uid)
      //   .OnDelete(DeleteBehavior.Cascade);
      // builder.Entity<v_test_question>().HasOne(p => p.test_detail).WithMany(p => p.test_questions).HasForeignKey(p => p.test_detail_uid)
      //   .OnDelete(DeleteBehavior.Cascade);
      
    }
  }
}
