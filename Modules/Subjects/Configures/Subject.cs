using Microsoft.Extensions.DependencyInjection;
using rmutp_mooc_api.Modules.CourseActivity.Services;
using rmutp_mooc_api.Modules.Course.Services;
using rmutp_mooc_api.Modules.Question.Services;

namespace rmutp_mooc_api.Modules.Subject.Configures
{
  public static class SubjectCollectionExtension
  {
    public static IServiceCollection AddSubjectServices(this IServiceCollection services)
    {
      //services.AddAutoMapper(typeof(ProfileMapping));
      services.AddScoped<QuestionService>();
      services.AddScoped<CourseActivityService>();
      services.AddScoped<CourseService>();
      // services.AddScoped<DynamicSubjectEducationTypeService>();
      return services;
    }
  }
}
