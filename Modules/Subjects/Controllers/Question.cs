using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rmutp_mooc_api.Modules.Question.Services;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Question.Controllers
{
  [Route("api/subject/question", Name = "question")]
  public class QuestionController:Controller
  {
    private readonly Db _db;
    private readonly QuestionService _entityUidService;
    public QuestionController(QuestionService entityUidService,Db db) 
    {
      _db = db;
      _entityUidService = entityUidService;
    }
    [HttpGet, Route("sync/{course_activity_uid}")]
    public async Task<ActionResult<List<v_question>>> GetSyncQuestion([FromRoute]Guid course_activity_uid)
    {
      return Ok(await _entityUidService.GetSyncQuestion(course_activity_uid));
    }
  }
}
