using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rmutp_mooc_api.Modules.CourseActivity.Services;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.CourseActivity.Controllers
{
  [Route("api/subject/course_activity", Name = "course_activity")]
  public class CourseActivityController:BaseUidController<t_course_activity,v_course_activity>
  {
    private readonly Db _db;
    private readonly CourseActivityService _entityUidService;
    public CourseActivityController(CourseActivityService entityUidService,Db db) : base(entityUidService)
    {
      _db = db;
      _entityUidService = entityUidService;
    }
    // [HttpGet, Route("detail/{course_activity_uid}")]
    // public async Task<ActionResult<List<v_question>>> GetCourseActivityDetail([FromRoute]Guid course_activity_uid)
    // {
    //   return Ok(await _entityUidService.GetEntityDetail(course_activity_uid));
    // }
  }
}
