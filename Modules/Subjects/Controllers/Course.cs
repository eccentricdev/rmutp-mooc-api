using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rmutp_mooc_api.Modules.Course.Services;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Course.Controllers
{
  [Route("api/subject/course", Name = "course")]
  public class CourseController:BaseUidController<t_course,v_course>
  {
    private readonly Db _db;
    private readonly CourseService _entityUidService;
    public CourseController(CourseService entityUidService,Db db) : base(entityUidService)
    {
      _db = db;
      _entityUidService = entityUidService;
    }
    // [HttpGet, Route("reorder/{course_uid}")]
    // public async Task<ActionResult<List<v_question>>> GetCourseDetail([FromRoute]Guid course_uid)
    // {
    //   return Ok(await _entityUidService.GetEntity(course_uid));
    // }
  }
}
