using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;

namespace rmutp_mooc_api.Modules.Question.Services
{
  public class QuestionService
  {
    private readonly Db _context;
    public QuestionService(Db context) 
    {
      _context = context;
    }

    public async Task<List<v_question>> GetSyncQuestion(Guid course_activity_uid)
    {
      var fromActivity = await _context.t_activity.FirstOrDefaultAsync(c =>c.activity_code == "pre-test");
      var courseActivity = await _context.t_course_activity.FirstOrDefaultAsync(c =>c.course_activity_uid == course_activity_uid);
      var activity = await _context.v_activity.FirstOrDefaultAsync(c=>c.activity_uid == courseActivity.activity_uid);
      if(activity!=null){
        if(activity.activity_code == "pre-test"){
          fromActivity = await _context.t_activity.FirstOrDefaultAsync(c =>c.activity_code == "post_test");
        }
      }
      var fromCourseActivity = await _context.v_course_activity.FirstOrDefaultAsync(c=>c.activity_uid == fromActivity.activity_uid && c.course_activity_uid == courseActivity.course_activity_uid);
      // var toCourseActivity = await _context.t_course_activity.FirstOrDefaultAsync(c =>
      //   c.activity_uid == toActivity.activity_uid &&
      //   c.course_uid == courseActivity.course_uid
      //   );
      var fromQuestions = await _context.t_question.AsNoTracking().Where(c =>c.course_activity_uid == fromCourseActivity.course_activity_uid ).ToListAsync();
      foreach(var question in fromQuestions){
        var questionDetail = await _context.t_question.FirstOrDefaultAsync(c =>
          c.question_uid == question.question_uid && 
          c.course_activity_uid == courseActivity.course_activity_uid
          );
        if(questionDetail == null){
            question.question_uid = Guid.NewGuid();
            question.course_activity_uid = courseActivity.course_activity_uid;
            await _context.t_question.AddAsync(question);
            await _context.SaveChangesAsync();
        }
      }
      return await _context.v_question.Where(c=>c.course_activity_uid == courseActivity.course_activity_uid).ToListAsync();
    }
  }
}
