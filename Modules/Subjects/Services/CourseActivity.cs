using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;

namespace rmutp_mooc_api.Modules.CourseActivity.Services
{
  public class CourseActivityService : EntityUidService<t_course_activity,v_course_activity>
  {
    private readonly Db _context;
    public CourseActivityService(Db context)  : base(context)
    {
      _context = context;
    }
    public override async Task<v_course_activity> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      // return await _context.v_course_activity.FirstOrDefaultAsync();
      return await _context.v_course_activity.Include(c=>c.questions).ThenInclude(c=>c.choices).FirstOrDefaultAsync(c=>c.course_activity_uid==uid);
    }
    // public async Task<v_course_activity> GetEntityDetail(Guid uid)
    // {
    //   return await _context.v_course_activity.Include(c=>c.questions).ThenInclude(c=>c.choices).FirstOrDefaultAsync(c=>c.course_activity_uid==uid);
    //   // var course_activity = await _context.v_course_activity.Include(c=>c.questions).FirstOrDefaultAsync(c=>c.course_uid==uid);
    //   // var questions = new List<v_question>();
    //   // Console.WriteLine(course_activity.questions.Count());
    //   // foreach(var question in course_activity.questions){
    //   //   var questionDetail = await _context.v_question.Include(c=>c.choices).FirstOrDefaultAsync(c=>c.question_uid == question.question_uid);
    //   //   questionDetail.choices = await _context.v_choice.Where(c=>c.question_uid == question.question_uid).ToListAsync();
    //   //   Console.WriteLine(questionDetail.choices.Count());
    //   //   questions.Add(questionDetail);
    //   // }
    //   // course_activity.questions = questions;
    //   // return course_activity;
    // }
  }
}
