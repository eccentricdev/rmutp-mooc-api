using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rmutp_mooc_api.Modules.Subject.Databases.Models;
using SeventyOneDev.Utilities;

namespace rmutp_mooc_api.Modules.Course.Services
{
  public class CourseService : EntityUidService<t_course,v_course>
  {
    private readonly Db _context;
    public CourseService(Db context)  : base(context)
    {
      _context = context;
    }

    public override async Task<v_course> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      var course = await _context.v_course.FirstOrDefaultAsync(c=>c.course_uid==uid);
      var course_activitys = await _context.v_course_activity.Where(c=>c.course_uid==uid).OrderBy(c=>c.row_no).ToListAsync();
      course.course_activitys = course_activitys;
      return course;
    }
  }
}
