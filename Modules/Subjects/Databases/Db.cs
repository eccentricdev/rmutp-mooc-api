using rmutp_mooc_api.Modules.Subject.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    
    public DbSet<t_subject> t_subject {get;set;}
    public DbSet<v_subject> v_subject {get;set;} 
    public DbSet<t_activity> t_activity {get;set;}
    public DbSet<v_activity> v_activity {get;set;} 
    public DbSet<t_course> t_course {get;set;}
    public DbSet<v_course> v_course {get;set;} 
    public DbSet<t_course_activity> t_course_activity {get;set;}
    public DbSet<v_course_activity> v_course_activity {get;set;} 
    public DbSet<t_question> t_question {get;set;}
    public DbSet<v_question> v_question {get;set;} 
    public DbSet<t_choice> t_choice {get;set;}
    public DbSet<v_choice> v_choice {get;set;} 
    public DbSet<t_skill> t_skill {get;set;}
    public DbSet<v_skill> v_skill {get;set;} 
    public DbSet<t_language> t_language {get;set;}
    public DbSet<v_language> v_language {get;set;} 
    private void OnSubjectModelCreating(ModelBuilder builder, string schema)
    {
      builder.Entity<t_subject>().ToTable("subject", schema);
      builder.Entity<v_subject>().ToView("v_subject", schema);
      
      builder.Entity<t_instructor>().ToTable("instructor", schema);
      builder.Entity<v_instructor>().ToView("v_instructor", schema);

      builder.Entity<t_instructor>().ToTable("instructor", schema);
      builder.Entity<v_instructor>().ToView("v_instructor", schema);

      builder.Entity<t_subject_instructor>().ToTable("subject_instructor", schema);
      builder.Entity<v_subject_instructor>().ToView("v_subject_instructor", schema);
      builder.Entity<t_subject_instructor>().HasOne(p => p.subject).WithMany(p => p.subject_instructors).HasForeignKey(p => p.subject_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_subject_instructor>().HasOne(p => p.subject).WithMany(p => p.subject_instructors).HasForeignKey(p => p.subject_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_subject_instructor>().HasOne(p => p.instructor).WithMany(p => p.subject_instructors).HasForeignKey(p => p.instructor_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_subject_instructor>().HasOne(p => p.instructor).WithMany(p => p.subject_instructors).HasForeignKey(p => p.instructor_uid)
        .OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_course>().ToTable("course", schema);
      builder.Entity<v_course>().ToView("v_course", schema);
      builder.Entity<v_course>().HasOne(p => p.subject).WithMany(p => p.courses).HasForeignKey(p => p.subject_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_course_activity>().ToTable("course_activity", schema);
      builder.Entity<v_course_activity>().ToView("v_course_activity", schema);
      builder.Entity<t_course_activity>().HasOne(p => p.course).WithMany(p => p.course_activitys).HasForeignKey(p => p.course_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_course_activity>().HasOne(p => p.course).WithMany(p => p.course_activitys).HasForeignKey(p => p.course_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_question>().ToTable("question", schema);
      builder.Entity<v_question>().ToView("v_question", schema);
      builder.Entity<t_question>().HasOne(p => p.course_activity).WithMany(p => p.questions).HasForeignKey(p => p.course_activity_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_question>().HasOne(p => p.course_activity).WithMany(p => p.questions).HasForeignKey(p => p.course_activity_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_choice>().ToTable("choice", schema);
      builder.Entity<v_choice>().ToView("v_choice", schema);
      builder.Entity<t_choice>().HasOne(p => p.question).WithMany(p => p.choices).HasForeignKey(p => p.question_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_choice>().HasOne(p => p.question).WithMany(p => p.choices).HasForeignKey(p => p.question_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_activity>().ToTable("activity", schema);
      builder.Entity<v_activity>().ToView("v_activity", schema);

      builder.Entity<t_skill>().ToTable("skill", schema);
      builder.Entity<v_skill>().ToView("v_skill", schema);

      builder.Entity<t_language>().ToTable("language", schema);
      builder.Entity<v_language>().ToView("v_language", schema);
      
    }
  }
}
