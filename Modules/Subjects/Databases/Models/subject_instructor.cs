using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class subject_instructor:base_table
  {
    [Key] public Guid? subject_instructor_uid { get; set; }
    public Guid? instructor_uid { get; set; }
    public Guid? subject_uid { get; set; }
  }
  [GeneratedUidController("api/subject/subject_instructor")]
  public class t_subject_instructor : subject_instructor
  {
    [JsonIgnore]
    public t_instructor instructor { get; set; }
    [JsonIgnore]
    public t_subject subject { get; set; }
  }

  public class v_subject_instructor : subject_instructor
  {
    [JsonIgnore]
    public v_instructor instructor { get; set; }
    [JsonIgnore]
    public v_subject subject { get; set; }
  }
}
