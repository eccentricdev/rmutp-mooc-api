using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class choice:base_table
  {
    [Key] public Guid? choice_uid { get; set; }
    public string choice_name { get; set; }
    public Guid? question_uid { get; set; }
    // [NotMapped]
    public Boolean? is_checked { get; set; }
  }
  [GeneratedUidController("api/subject/choice")]
  public class t_choice : choice
  {
    [JsonIgnore]
    public t_question question { get; set; }
  }

  public class v_choice : choice
  {
    
    [JsonIgnore]
    public v_question question { get; set; }
  }
}
