using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class course:base_table
  {
    [Key] public Guid? course_uid { get; set; }
    public Guid? subject_uid { get; set; }
    public string course_code { get; set; }
    public string course_short_name_en { get; set; }
    public string course_short_name_th { get; set; }
    public string course_name_th { get; set; }
    public string course_name_en { get; set; }
    public string course_image { get; set; }
    public int? row_no { get; set; }
  }
  // [GeneratedUidController("api/subject/course")]
  public class t_course : course
  {
    [Include]
    public List<t_course_activity> course_activitys { get; set; }
  }

  public class v_course : course
  {
    [JsonIgnore]
    public v_subject subject { get; set; }
    [Include]
    public List<v_course_activity> course_activitys { get; set; }
  }
}
