using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class language:base_table
  {
    [Key] public Guid? language_uid { get; set; }
    public string language_code { get; set; }
    public string language_short_name_en { get; set; }
    public string language_short_name_th { get; set; }
    public string language_name_th { get; set; }
    public string language_name_en { get; set; }
    public string language_image { get; set; }
  }
  [GeneratedUidController("api/subject/language")]
  public class t_language : language
  {
    // [JsonIgnore]
    // public List<t_question> questions { get; set; }
  }

  public class v_language : language
  {
    // [JsonIgnore]
    // public List<v_question> questions { get; set; }
  }
}
