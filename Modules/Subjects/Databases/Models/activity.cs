using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class activity:base_table
  {
    [Key] public Guid? activity_uid { get; set; }
    public string activity_code { get; set; }
    public string activity_short_name_en { get; set; }
    public string activity_short_name_th { get; set; }
    public string activity_name_th { get; set; }
    public string activity_name_en { get; set; }
    public string activity_image { get; set; }
  }
  [GeneratedUidController("api/subject/activity")]
  public class t_activity : activity
  {
    // [JsonIgnore]
    // public List<t_question> questions { get; set; }
  }

  public class v_activity : activity
  {
    // [JsonIgnore]
    // public List<v_question> questions { get; set; }
  }
}
