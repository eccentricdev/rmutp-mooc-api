using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class question:base_table
  {
    [Key] public Guid? question_uid { get; set; }
    public string question_name { get; set; }
    public string correct_answer { get; set; }
    public Guid? correct_choice_uid { get; set; }
    public Guid? course_activity_uid { get; set; }
  }
  [GeneratedUidController("api/subject/question")]
  public class t_question : question
  {
    [JsonIgnore]
    public t_course_activity course_activity { get; set; }
    [Include]
    public List<t_choice> choices { get; set; }
  }

  public class v_question : question
  {
    [JsonIgnore]
    public v_course_activity course_activity { get; set; }
    [Include]
    public List<v_choice> choices { get; set; }
  }
}
