using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class instructor:base_table
  {
    [Key] public Guid? instructor_uid { get; set; }
    public string instructor_code { get; set; }
    public string instructor_short_name_en { get; set; }
    public string instructor_short_name_th { get; set; }
    public string instructor_name_th { get; set; }
    public string instructor_name_en { get; set; }
    public string instructor_image { get; set; }
    public string instructor_description_history { get; set; }
    public string faculty_name { get; set; }
  }
  [GeneratedUidController("api/subject/instructor")]
  public class t_instructor : instructor
  {
    [Include]
    public List<t_subject_instructor> subject_instructors { get; set; }
  }

  public class v_instructor : instructor
  {
    [Include]
    public List<v_subject_instructor> subject_instructors { get; set; }
  }
}
