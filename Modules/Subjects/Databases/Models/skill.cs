using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class skill:base_table
  {
    [Key] public Guid? skill_uid { get; set; }
    public string skill_code { get; set; }
    public string skill_short_name_en { get; set; }
    public string skill_short_name_th { get; set; }
    public string skill_name_th { get; set; }
    public string skill_name_en { get; set; }
    public string skill_image { get; set; }
  }
  [GeneratedUidController("api/subject/skill")]
  public class t_skill : skill
  {
    // [JsonIgnore]
    // public List<t_question> questions { get; set; }
  }

  public class v_skill : skill
  {
    // [JsonIgnore]
    // public List<v_question> questions { get; set; }
  }
}
