using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class subject:base_table
  {
    [Key] public Guid? subject_uid { get; set; }
    public string subject_code { get; set; }
    public string subject_short_name_en { get; set; }
    public string subject_short_name_th { get; set; }
    public string subject_name_th { get; set; }
    public string subject_name_en { get; set; }
    public string subject_image { get; set; }
    public string subject_banner_image { get; set; }
    public string faculty_name { get; set; }
    public string instructor_name { get; set; }
    public Guid? language_uid { get; set; }
    public Guid? skill_uid { get; set; }
    public Boolean? is_certificate_reward { get; set; }
    public string subject_description_detail { get; set; }
    public string subject_purpose { get; set; }
    public string subject_condition { get; set; }
    public int? study_hour { get; set; }
    public int? course_number { get; set; }
  }
  [GeneratedUidController("api/subject/subject")]
  public class t_subject : subject
  {
    [Include]
    public List<t_subject_instructor> subject_instructors { get; set; }
  }

  public class v_subject : subject
  {
    [Include]
    public List<v_subject_instructor> subject_instructors { get; set; }
    [Include]
    public List<v_course> courses { get; set; }
  }
}
