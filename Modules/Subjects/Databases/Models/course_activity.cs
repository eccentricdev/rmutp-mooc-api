using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rmutp_mooc_api.Modules.Subject.Databases.Models
{
  public class course_activity:base_table
  {
    [Key] public Guid? course_activity_uid { get; set; }
    public string course_activity_code { get; set; }
    public string course_activity_short_name_en { get; set; }
    public string course_activity_short_name_th { get; set; }
    public string course_activity_name_th { get; set; }
    public string course_activity_name_en { get; set; }
    public string course_activity_image { get; set; }
    public string source_learning_video { get; set; }
    public int? test_time { get; set; }
    public int test_question_number { get; set; }
    public int minimum_score_percent { get; set; }
    public int? row_no { get; set; }
    public Guid? activity_uid { get; set; }
    public Guid? course_uid { get; set; }
  }
  // [GeneratedUidController("api/subject/course_activity")]
  public class t_course_activity : course_activity
  {
    [JsonIgnore]
    public t_course course { get; set; }
    [Include]
    public List<t_question> questions { get; set; }
  }

  public class v_course_activity : course_activity
  {
    [JsonIgnore]
    public v_course course { get; set; }
    [Include]
    public List<v_question> questions { get; set; }
  }
}
