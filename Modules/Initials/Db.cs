using Microsoft.EntityFrameworkCore;
using rmutp_mooc_api.Databases.Models;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_status> t_status { get; set; }
    public DbSet<v_status> v_status { get; set; }
   
    private void OnCommonModelCreating(ModelBuilder builder,string schema)
    {
      // builder.Entity<t_prefix>().ToTable("prefix", schema);
      // builder.Entity<v_prefix>().ToView("v_prefix", schema);
      // builder.Entity<t_blood_group>().ToTable("blood_group", schema);
      //
      // builder.Entity<t_religion>().ToTable("religion", schema)

      builder.Entity<t_status>().ToTable("status", schema);
      builder.Entity<v_status>().ToView("v_status",schema);
    }
  }
}
