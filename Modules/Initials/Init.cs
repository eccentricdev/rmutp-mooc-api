using System.Linq;
using SeventyOneDev.Utilities;
using rmutp_mooc_api.Databases;

namespace rmutp_mooc_api.Modules.Commons.Initials
{
  public static class CommonInit
  {
    public static void Run(Db context)
    {
      if (!context.t_status.Any())
      {
        context.t_status.AddRange(Status.Get());
        context.SaveChanges();
      }
      

    }
  }
}
