

using System.Collections.Generic;

namespace SeventyOneDev.Utilities
{
  public class Error
  {
    public int id { get; set; }
    public string description { get; set; }
    public List<string> requires { get; set; }

    public Error(int id, string description)
    {
      this.id = id;
      this.description = description;
    }
  }
  public static class ErrorMessage
  {
    public static Error Get(string code)
    {
      return code switch
      {
        "NEW_DUPLICATE" => new Error(30003, "ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากมีข้อมูลอยู่แล้ว"),
        "UPDATE_DUPLICATE" => new Error(30002, "ไม่สามารถแก้ไขข้อมูลได้ เนื่องจากมีข้อมูลอยู่แล้ว"),
        "INUSE" => new Error(30001, "ไม่สามารถลบได้ เนื่องจากมีการใช้งานอยู่"),
        _ => new Error(30000, "ไม่สามารถใช้งานได้ โปรดติดต่อผู้ดูแลระบบ")
      };
    }

  }
}
