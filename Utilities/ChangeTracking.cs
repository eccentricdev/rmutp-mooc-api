using System;

namespace SeventyOneDev.Utilities
{
  public class ChangeTracking
  {

    public string class_name { get; set; }
    public string property_name { get; set; }
    public Guid key { get; set; }
    public string old_value { get; set; }
    public string new_value { get; set; }
    public DateTime date_changed { get; set; }

  }
}
