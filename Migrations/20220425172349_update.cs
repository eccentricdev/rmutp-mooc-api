﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace rmutp_mooc_api.Migrations
{
    public partial class update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "RMUTP");

            migrationBuilder.CreateTable(
                name: "activity",
                schema: "RMUTP",
                columns: table => new
                {
                    activity_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    activity_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    activity_short_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    activity_short_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    activity_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    activity_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    activity_image = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_activity", x => x.activity_uid);
                });

            migrationBuilder.CreateTable(
                name: "course",
                schema: "RMUTP",
                columns: table => new
                {
                    course_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    subject_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    course_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    course_short_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    course_short_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    course_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    course_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    course_image = table.Column<string>(type: "text", nullable: true),
                    row_no = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course", x => x.course_uid);
                });

            migrationBuilder.CreateTable(
                name: "language",
                schema: "RMUTP",
                columns: table => new
                {
                    language_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    language_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    language_short_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    language_short_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    language_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    language_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    language_image = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_language", x => x.language_uid);
                });

            migrationBuilder.CreateTable(
                name: "skill",
                schema: "RMUTP",
                columns: table => new
                {
                    skill_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    skill_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    skill_short_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    skill_short_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    skill_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    skill_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    skill_image = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_skill", x => x.skill_uid);
                });

            migrationBuilder.CreateTable(
                name: "status",
                schema: "RMUTP",
                columns: table => new
                {
                    status_id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    status_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    status_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    status_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_status", x => x.status_id);
                });

            migrationBuilder.CreateTable(
                name: "subject",
                schema: "RMUTP",
                columns: table => new
                {
                    subject_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    subject_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    subject_short_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    subject_short_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    subject_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    subject_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    subject_image = table.Column<string>(type: "text", nullable: true),
                    faculty_name = table.Column<string>(type: "text", nullable: true),
                    instructor_name = table.Column<string>(type: "text", nullable: true),
                    language_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    skill_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    is_certificate_reward = table.Column<bool>(type: "boolean", nullable: true),
                    subject_description = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    subject_purpose = table.Column<string>(type: "text", nullable: true),
                    study_hour = table.Column<int>(type: "integer", nullable: true),
                    course_number = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_subject", x => x.subject_uid);
                });

            migrationBuilder.CreateTable(
                name: "test_certificate",
                schema: "RMUTP",
                columns: table => new
                {
                    test_certificate_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    test_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    user_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    is_agree = table.Column<bool>(type: "boolean", nullable: true),
                    is_complete = table.Column<bool>(type: "boolean", nullable: true),
                    citizen_no = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    image = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_test_certificate", x => x.test_certificate_uid);
                });

            migrationBuilder.CreateTable(
                name: "test_subject",
                schema: "RMUTP",
                columns: table => new
                {
                    test_subject_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    user_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    subject_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    test_subject_no = table.Column<int>(type: "integer", nullable: true),
                    test_subject_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    remark = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: true),
                    is_complete = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_test_subject", x => x.test_subject_uid);
                });

            migrationBuilder.CreateTable(
                name: "user",
                schema: "RMUTP",
                columns: table => new
                {
                    user_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    email = table.Column<string>(type: "text", nullable: true),
                    full_name = table.Column<string>(type: "text", nullable: true),
                    password = table.Column<string>(type: "text", nullable: true),
                    image = table.Column<string>(type: "text", nullable: true),
                    faculty_name = table.Column<string>(type: "text", nullable: true),
                    major_name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.user_uid);
                });

            migrationBuilder.CreateTable(
                name: "course_activity",
                schema: "RMUTP",
                columns: table => new
                {
                    course_activity_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    course_activity_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    course_activity_short_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    course_activity_short_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    course_activity_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    course_activity_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    course_activity_image = table.Column<string>(type: "text", nullable: true),
                    source_learning_video = table.Column<string>(type: "text", nullable: true),
                    test_time = table.Column<int>(type: "integer", nullable: true),
                    test_question_number = table.Column<int>(type: "integer", nullable: false),
                    minimum_score_percent = table.Column<int>(type: "integer", nullable: true),
                    row_no = table.Column<int>(type: "integer", nullable: true),
                    activity_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    course_uid = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_activity", x => x.course_activity_uid);
                    table.ForeignKey(
                        name: "FK_course_activity_course_course_uid",
                        column: x => x.course_uid,
                        principalSchema: "RMUTP",
                        principalTable: "course",
                        principalColumn: "course_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "test",
                schema: "RMUTP",
                columns: table => new
                {
                    test_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    test_subject_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    course_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    remark = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: true),
                    is_complete = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_test", x => x.test_uid);
                    table.ForeignKey(
                        name: "FK_test_test_subject_test_subject_uid",
                        column: x => x.test_subject_uid,
                        principalSchema: "RMUTP",
                        principalTable: "test_subject",
                        principalColumn: "test_subject_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "question",
                schema: "RMUTP",
                columns: table => new
                {
                    question_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    question_name = table.Column<string>(type: "text", nullable: true),
                    correct_answer = table.Column<string>(type: "text", nullable: true),
                    correct_choice_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    course_activity_uid = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_question", x => x.question_uid);
                    table.ForeignKey(
                        name: "FK_question_course_activity_course_activity_uid",
                        column: x => x.course_activity_uid,
                        principalSchema: "RMUTP",
                        principalTable: "course_activity",
                        principalColumn: "course_activity_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "test_detail",
                schema: "RMUTP",
                columns: table => new
                {
                    test_detail_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    test_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    course_activity_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    is_complete = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_test_detail", x => x.test_detail_uid);
                    table.ForeignKey(
                        name: "FK_test_detail_test_test_uid",
                        column: x => x.test_uid,
                        principalSchema: "RMUTP",
                        principalTable: "test",
                        principalColumn: "test_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "choice",
                schema: "RMUTP",
                columns: table => new
                {
                    choice_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    choice_name = table.Column<string>(type: "text", nullable: true),
                    question_uid = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_choice", x => x.choice_uid);
                    table.ForeignKey(
                        name: "FK_choice_question_question_uid",
                        column: x => x.question_uid,
                        principalSchema: "RMUTP",
                        principalTable: "question",
                        principalColumn: "question_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "test_question",
                schema: "RMUTP",
                columns: table => new
                {
                    test_question_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<short>(type: "smallint", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    test_detail_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    question_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    selected_choice_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    test_question_answer = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_test_question", x => x.test_question_uid);
                    table.ForeignKey(
                        name: "FK_test_question_test_detail_test_detail_uid",
                        column: x => x.test_detail_uid,
                        principalSchema: "RMUTP",
                        principalTable: "test_detail",
                        principalColumn: "test_detail_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_choice_question_uid",
                schema: "RMUTP",
                table: "choice",
                column: "question_uid");

            migrationBuilder.CreateIndex(
                name: "IX_course_activity_course_uid",
                schema: "RMUTP",
                table: "course_activity",
                column: "course_uid");

            migrationBuilder.CreateIndex(
                name: "IX_question_course_activity_uid",
                schema: "RMUTP",
                table: "question",
                column: "course_activity_uid");

            migrationBuilder.CreateIndex(
                name: "IX_test_test_subject_uid",
                schema: "RMUTP",
                table: "test",
                column: "test_subject_uid");

            migrationBuilder.CreateIndex(
                name: "IX_test_detail_test_uid",
                schema: "RMUTP",
                table: "test_detail",
                column: "test_uid");

            migrationBuilder.CreateIndex(
                name: "IX_test_question_test_detail_uid",
                schema: "RMUTP",
                table: "test_question",
                column: "test_detail_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "activity",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "choice",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "language",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "skill",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "status",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "subject",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "test_certificate",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "test_question",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "user",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "question",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "test_detail",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "course_activity",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "test",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "course",
                schema: "RMUTP");

            migrationBuilder.DropTable(
                name: "test_subject",
                schema: "RMUTP");
        }
    }
}
