# Build Stage
FROM mcr.microsoft.com/dotnet/sdk:5.0 as build-env
WORKDIR /source
COPY . .
# RUN git clone https://chokchai_j:jcuJe8JgCUVG8YNYSbAk@bitbucket.org/eccentricdev/base_api.git
RUN git clone https://chokchai_j:jcuJe8JgCUVG8YNYSbAk@bitbucket.org/eccentricdev/core_api.git
# RUN git clone https://chokchai_j:jcuJe8JgCUVG8YNYSbAk@bitbucket.org/eccentricdev/master_core.git
RUN dotnet restore --configfile nuget.config
RUN dotnet build
RUN dotnet publish -o /publish --configuration Release;
# Publish Stage
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build-env /publish .
ENV ASPNETCORE_URLS http://*:8080
ENTRYPOINT ["dotnet", "rmutp-mooc-api.dll"]
